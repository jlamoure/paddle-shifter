#include <TimedAction.h>

int wheelSpeed = 0;
int gearRead = 0;
int up = 0;
int down = 0;
int clutch = 0;
int gear = 0;
int wheelSpeedBefore = 0;
int buzzCount = 0;

bool tooFast = 0;
bool tooSlow = 0;
bool buzzState = 0;
bool buzzEnable = 0;
bool upShifted = 0;
bool downShifted = 0;
bool locked = 0;

const int wheelPollRate = 250;
const int upShiftDelay = 500;
const int downShiftDelay = 100;
const int clutchDelay = 100;

const int upLED = 12;
const int upBtn = 11;
const int downLED = 2;
const int downBtn = 3;
const int clutchLED = 7;
const int clutchBtn = 8;
const int moneyLED = 6;
const int stallLED = 9;
const int buzzer = 13;

const int speedPot = 0;
const int speedLED = 10;

const int gearPot = 5;
const int gearLED0 = 5;
const int gearLED1 = 4;

/*#############################################FUNCTIONS###################################*/

void RateCalculate()
{
  wheelSpeedBefore = wheelSpeed;
}

void Buzz()
{
  if (buzzState == false)
  {
    buzzState = true;
    digitalWrite(buzzer,LOW);  
  }
  else
  {
    buzzState = false;
    digitalWrite(buzzer,HIGH); 
  }
  buzzCount ++;
}

void ClutchFunc()
{
  if (clutch == LOW)
  {
    digitalWrite(clutchLED, HIGH);
  }
  else
  {
    digitalWrite(clutchLED, LOW);
  }
}

/*#############################################^^^FUNCTIONS^^^###################################*/

TimedAction wheelSpeedRate = TimedAction(wheelPollRate,RateCalculate);
TimedAction buzzAction = TimedAction(50, Buzz);

void setup() 
{
  pinMode(upLED, OUTPUT);
  pinMode(upBtn, INPUT_PULLUP);
  pinMode(downLED, OUTPUT);
  pinMode(downBtn, INPUT_PULLUP);
  pinMode(clutchLED, OUTPUT);
  pinMode(clutchBtn, INPUT_PULLUP);
  pinMode(moneyLED, OUTPUT);
  pinMode(stallLED, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(gearLED0, OUTPUT);
  pinMode(gearLED1, OUTPUT);
  //attachInterrupt(digitalPinToInterrupt(13 or 2/3), ClutchFunc, LOW);
 // Serial.begin(9600);
}

void loop() 
{
  wheelSpeedRate.check();
  wheelSpeed = analogRead(speedPot);
  gearRead = analogRead(gearPot);
  up = digitalRead(upBtn);
  down = digitalRead(downBtn);
  clutch = digitalRead(clutchBtn);
  /*Serial.print(wheelSpeed);
  Serial.print("\t");
  Serial.print(gearRead);
  Serial.print("\t");
  Serial.print(up);
  Serial.print("\t");
  Serial.print(down);
  Serial.print("\t");
  Serial.print(clutch);
  Serial.print("\t");
  Serial.print(gear);
  Serial.print("\t");
  Serial.print(wheelSpeed-wheelSpeedBefore);
  Serial.print("\t");
  Serial.print(locked);
  Serial.println();*/

/*####################################################Money Shift Checker##################################################*/

  if (wheelSpeed-wheelSpeedBefore < -20)
  {
    locked = true;
    buzzEnable = true;
  }
  else if (wheelSpeed-wheelSpeedBefore >= -20)
  {
    locked = false;
  }

  if (buzzEnable == true)
  {
    buzzAction.check();  
  }

  if (buzzCount == 10)
  { 
    buzzEnable = false;
    buzzCount = 0;
    digitalWrite(buzzer,LOW);
  }
  
  if (gearRead < 256)
  {
    digitalWrite(gearLED0, LOW);
    digitalWrite(gearLED1, LOW);
    gear = 1; 
  }
  else if ((gearRead >256) && (gearRead <512))
  {
    digitalWrite(gearLED0, HIGH);
    digitalWrite(gearLED1, LOW);
    gear = 2; 
  }
  else if ((gearRead >512) && (gearRead <768))
  {
    digitalWrite(gearLED0, LOW);
    digitalWrite(gearLED1, HIGH); 
    gear = 3;
  }
  else
  {
    digitalWrite(gearLED0, HIGH);
    digitalWrite(gearLED1, HIGH); 
    gear = 4;
  }
 
  analogWrite(speedLED, wheelSpeed/4);

  if (gear == 1 && wheelSpeed < 50)
  {
    digitalWrite(moneyLED, LOW);
    digitalWrite(stallLED, HIGH);
    tooFast = 0;
    tooSlow = 1;
  }
  else if (gear == 1 && wheelSpeed >= 50)
  {
    digitalWrite(moneyLED, LOW);
    digitalWrite(stallLED, LOW);
    tooFast = 0;
    tooSlow = 0;
  }
  else if (gear == 2 && wheelSpeed < 60)
  {
    digitalWrite(moneyLED, LOW);
    digitalWrite(stallLED, HIGH);
    tooFast = 0;
    tooSlow = 1;
  }
  else if (gear == 2 && wheelSpeed >= 60 && wheelSpeed < 290)
  {
    digitalWrite(moneyLED, LOW);
    digitalWrite(stallLED, LOW);
    tooFast = 0;
    tooSlow = 0;
  }
  else if (gear == 2 && wheelSpeed >= 290)
  {
    digitalWrite(moneyLED, HIGH);
    digitalWrite(stallLED, LOW);
    tooFast = 1;
    tooSlow = 0;
  }
  else if (gear == 3 && wheelSpeed < 90)
  {
    digitalWrite(moneyLED, LOW);
    digitalWrite(stallLED, HIGH);
    tooFast = 0;
    tooSlow = 1;
  }
  else if (gear == 3 && wheelSpeed >= 90 && wheelSpeed <480)
  {
    digitalWrite(moneyLED, LOW);
    digitalWrite(stallLED, LOW);
    tooFast = 0;
    tooSlow = 0;
  }
  else if (gear == 3 && wheelSpeed >= 480)
  {
    digitalWrite(moneyLED, HIGH);
    digitalWrite(stallLED, LOW);
    tooFast = 1;
    tooSlow = 0;
  }
  else if (gear == 4 && wheelSpeed < 720)
  {
    digitalWrite(moneyLED, LOW);
    digitalWrite(stallLED, LOW);
    tooFast = 0;
    tooSlow = 0;
  }
  else if (gear == 4 && wheelSpeed >= 720)
  {
    digitalWrite(moneyLED, HIGH);
    digitalWrite(stallLED, LOW);
    tooFast = 1;
    tooSlow = 0;
  }

/*##############################################################^^^MONEY SHIFT CHCKER ^^#########################################*/

/*#################################################################UP SHIFT PROCEDURE###################################################*/
  if (up == LOW && gear == 4 && upShifted == false)
  {
    buzzEnable = true;
    upShifted = true;
  }
  else if (up == LOW && tooSlow == true && upShifted == false)
  {
    buzzEnable = true;
    upShifted = true;
  }
  else if (up == LOW && upShifted == false && locked == false)
  {
    ClutchFunc();
    digitalWrite(upLED, HIGH);
    delay(upShiftDelay);
    digitalWrite(upLED, LOW);
    upShifted = true;
  }
  else if (up == HIGH)
  {
    upShifted = false;
  }
/*##################################################################^^^^ UP SHIFT PROCEDURE ^^^^^##################################################*/

/*##################################################################### DOWN SHIFT PROCEDURE #####################################################*/
  if (down == LOW && gear == 1 && downShifted == false)
  {
    buzzEnable = true;
    downShifted = true;
  }
  else if (down == LOW && tooFast == true && downShifted == false)
  {
    buzzEnable = true;
    downShifted = true;
  }
  else if (down == LOW && downShifted == false && locked == false)
  {
    digitalWrite(clutchLED, HIGH);
    delay(clutchDelay);
    digitalWrite(downLED, HIGH);
    delay(downShiftDelay);
    digitalWrite(downLED, LOW);
    delay(clutchDelay);
    ClutchFunc();
    downShifted = true;
  }
  else if (down == HIGH)
  {
    downShifted = false;
  }

  ClutchFunc();
/*#####################################################################^^^^^ DOWN SHIFT PROCEDURE ^^^^####################################################*/ 

}